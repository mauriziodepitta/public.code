This repo contains code used in simulations of work published by Maurizio De Pitta' and collaborators.

The code is organized by branches, each branch being associated to a publication. Unless differently stated, the code in each branch is self-contained. Please refer to the below "Branch list" to locate the code branch of your interest.    

The code in this repo is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

#################
# Branch List 
#################

1. De Pittà M., Brunel N., [*Modulation of synaptic plasticity by glutamatergic gliotransmission: A modeling study*](http://www.hindawi.com/journals/np/2016/7607924/). Neural Plasticity (2016). 
```
git clone https://mauriziodepitta@bitbucket.org/mauriziodepitta/public.code.git -b NeuralPlasticity-2016
```

#################
# Contact 
#################
Maurizio De Pitta'

https://sites.google.com/site/mauriziodepitta/home

maurizio-dot-depitta -at- gmail-dot-com